# Project Name
Elastic Prompt Search


## Description
The project is designed to find the most relevant prompt from a given dataset. It includes functionality for Elasticsearch integration, an Audit Log model to store search logs, and an Elasticsearch initializer to set up the connection.


## Features
Audit Log Model: Stores audit logs of searched prompts and content responses.

Elasticsearch Model: Supports Elasticsearch functionality for prompt search.

Elasticsearch Initializer: Sets up the connection with Elasticsearch before the Ruby server is up.

Database Seed: To populate the database with initial data using the seeds.rb file.

## Local Setup

### Prerequisites
- Ruby (version 3.2.3)
- Rails (version 7.1.3.2)
- Elasticsearch (version 7.2)
- Docker

### Create .env File
- Create a file named .env in the root of your project to store environment variables.

#### .env
- SECRET_KEY_BASE=7bbeae3fde912b49fab56265347bbe91
- POSTGRES_USER=postgres
- POSTGRES_DB=postgres
- POSTGRES_PASSWORD=promptfinder
- POSTGRES_HOST=192.168.32.3
- ELASTICSEARCH_URL=http://elasticsearch:9200

### Steps to Run Locally

1. Clone the repository:
   ```bash
   - git clone https://gitlab.com/FiftyFiveTech/elastic-prompt-search.git
   - cd your-project

2. Install dependencies:
    - bundle install
3. Set up the database:
    - rails db:create
    - rails db:migrate
    - rails db:seed
4. Start the Rails server:
    - rails server
5. Open your browser and visit http://localhost:3000 to access the application.

### Running with Docker

1. docker compose up --build (for elasticsearch + postgres)
2. docker compose -f docker-compose-prompt.yaml up --build (for elastic-prompt-search)



