require 'json'

class PromptsController < ApplicationController

  # GET /prompts/1 or /prompts/1.json
  def index
    # Get search query from URL parameters
    query = params[:query]

    # If query is provided, search Elasticsearch
    if query.present?
      # Perform search using Elasticsearch's search method with params
      prompts = Prompt.__elasticsearch__.search(query).response

      # Parse response JSON and extract content from hits
      parsed_json = JSON.parse(prompts.to_json)
      @prompts = parsed_json['hits']['hits'].map { |hit| hit['_source']['content'] }

      # Create audit log if prompts found
      if @prompts.present?
        @audit_log = AuditLog.new(search_term: query, content: @prompts)
        @audit_log.save
      end
    end
  end
end
