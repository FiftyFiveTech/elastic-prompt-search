# Define a Ruby on Rails base class named ApplicationRecord, inheriting from ActiveRecord::Base
class ApplicationRecord < ActiveRecord::Base
  # Designate this class as a primary abstract class
  primary_abstract_class
end
