# Define a Ruby on Rails model named AuditLog, inheriting from ApplicationRecord
class AuditLog < ApplicationRecord
    # Validate uniqueness of the search_term attribute
    validates :search_term, uniqueness: true
  end
  