# Import the Elasticsearch Model module for integrating Elasticsearch with the model
require 'elasticsearch/model'

# Define a Ruby on Rails model named Prompt, inheriting from ApplicationRecord
class Prompt < ApplicationRecord
  # Include Elasticsearch::Model module to enable Elasticsearch integration
  include Elasticsearch::Model

  # Include Elasticsearch::Model::Callbacks to enable callbacks for Elasticsearch actions
  include Elasticsearch::Model::Callbacks
end
