# Require necessary libraries
require 'elasticsearch/model'

# Configure Elasticsearch client
Elasticsearch::Model.client = Elasticsearch::Client.new(
  url: ENV['ELASTICSEARCH_URL'],
  request_timeout: 5,
  transport_options: {
    ssl: { verify: false },  # Disable SSL verification for testing (not recommended in production)
    headers: { 'Content-Type' => 'application/json' },
    request: { timeout: 5 }
  },
  user: "elastic",
  password: "1PxMVQIqGI7k7+CHaJNC",
  logger: Rails.logger
)
