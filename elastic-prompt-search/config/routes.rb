Rails.application.routes.draw do
  # Route for searching prompts
  # Map GET requests to "/search/prompt" to "PromptsController#index" action
  get "/search/prompt", to: "prompts#index"

  root "prompts#index"
end