class CreateAuditLogs < ActiveRecord::Migration[7.1]
  def change
    create_table :audit_logs do |t|
      t.string :search_term
      t.text :content, array: true, default: []

      t.timestamps
    end
  end
end
