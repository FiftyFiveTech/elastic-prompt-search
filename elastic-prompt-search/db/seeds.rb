# Require necessary libraries
require 'elasticsearch/model'
require 'net/http'
require 'json'
 
# Configure Elasticsearch client
@client = Elasticsearch::Client.new(
  url: ENV['ELASTICSEARCH_URL'] || "https://localhost:9200",
  request_timeout: 5,
  transport_options: {
    ssl: { verify: false },  # Disable SSL verification for testing (not recommended in production)
    headers: { 'Content-Type' => 'application/json' },
    request: { timeout: 5 }
  },
  user: "elastic",
  password: "1PxMVQIqGI7k7+CHaJNC",
  logger: Rails.logger
)
 
@index_name = 'prompts'
 
# create index if not exists
def create_index
  if @client.indices.exists(index: @index_name)
    puts "Index '#{@index_name}' already exists."
  else
    # Create the index with the settings and mappings
    @client.indices.create(index: @index_name)
    puts "Index '#{@index_name}' created."
  end
end
 
#  seed data in index
def seed_data
# Define URI
  uri = URI('https://datasets-server.huggingface.co/rows?dataset=Gustavosta%2FStable-Diffusion-Prompts&config=default&split=train&offset=0&length=100')
 
  # Send GET request to retrieve prompts
  response = Net::HTTP.get_response(uri)
 
  # Handle successful response (HTTP 200)
  if response.code == "200"
    # Delete all existing prompts
    deleted = Elasticsearch::Model.client.delete_by_query({
      index: "prompts",
      body: {
        query: {
          match_all: {}
        }
      }
    })
    puts "Deleted prompts: #{deleted.to_s}"
 
    # Parse JSON response
    data = JSON.parse(response.body)
 
    # Extract prompts from the response
    prompts = data['rows'].map { |row| row['row']['Prompt'] }
 
    # Create bulk request body
    prompts_arr = prompts.map do |prompt|
      "#{({ index: { _index: 'prompts' } }).to_json}\n#{({ content: prompt }).to_json}\n"
    end
    bulk_request_body = prompts_arr.join
 
    # Send bulk request to index prompts in Elasticsearch
    response = Elasticsearch::Model.client.bulk(body: bulk_request_body)
 
    # Handle successful bulk indexing
    if response['errors'] == false
      puts "Successfully indexed #{prompts.size} prompts."
    else
      puts "Bulk indexing errors: #{response['errors']}"
    end
 
  else
    # Handle failed HTTP request
    puts "HTTP request failed with status code: #{response.code}"
  end
end
 
 
# calling
create_index
seed_data
